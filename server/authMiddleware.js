const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];

  console.log(req.body);
  let noSlashPath = req.path.split('/')[1];

  if(noSlashPath.toString() === 'register' || noSlashPath.toString() === 'login') {
    return next();
  }

  if (token) {

    jwt.verify(token, 'secret', function(err, decoded) {      
      if (err) {
				console.log(err);
        return res.send('Failed to authenticate token.');    
      } else {
        req.decoded = decoded;
        next();
      }
    });

  } else {

    return res.status(403).send('No token provided.');

  }
};

module.exports = authMiddleware;