const mongoose = require('mongoose');

const db = {
	init: (dbUrl) => {
		mongoose.connect(dbUrl);
		
		let db = mongoose.connection;
		db.on('error', console.error.bind(console, 'Connection error:'));
		db.once('open', function() {
		  console.log('Connected to MongoDB!');
		});
	}
};

const models = {
	Booking: () => {
		let bookingSchema = mongoose.Schema({
			startDate: { type: Date, default: Date.now() },
			endDate: Date,
			startMileage: Number,
			endMileage: Number
		});

		return mongoose.model('Booking', bookingSchema);
	},

	User: () => {
		let userSchema = mongoose.Schema({
			username: { type: String, required: true },
			password: { type: String, required: true },
			email:    { type: String, required: true }
		});

		return mongoose.model('User', userSchema);
	}
};

module.exports = {db, models};