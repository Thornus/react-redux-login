const express = require('express');
const bodyParser = require('body-parser');
const {db} = require('./db');
const router = require('./router');
const authMiddleware = require('./authMiddleware');
const cors = require('cors');

db.init('mongodb://localhost');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(authMiddleware);

router.post(app);
router.get(app);
router.put(app);
router.delete(app);

app.listen(3001, () => console.log('Server listening on port 3001!'));