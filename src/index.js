import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createHistory from 'history/createBrowserHistory';
import reducer from './reducers/index';

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
  reducer,
  applyMiddleware(middleware)
);

ReactDOM.render(
	<Provider store={store}>
    <ConnectedRouter history={history}>
		  <App/>
	  </ConnectedRouter>
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();
