const defaultState = {};

export default function reducer(state = defaultState, action) {

	switch (action.type) {
		case 'LOGIN':
			return {
				...state,
				username: action.payload.username,
				email: action.payload.email,
				loggedIn: action.payload.loggedIn
			};

		case 'WRONG_CREDS':
			return {
				...state,
				message: action.message
			};

		default:
			return state;
	}

}
