import React, { Component } from 'react';
import config from '../config';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';

class User extends Component {
  render() {
    return (
        <div className='container'>
            <div className='field'>Username: </div><span>{this.props.username}</span>
            <div className='field'>Email: </div><span>{this.props.email}</span>
    	</div>
    );
  }
}

const mapStateToProps = state => {
  return {
		username: state.login.username,
        email: state.login.email
	};
};

const mapDispatchToProps = null;

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(User));
