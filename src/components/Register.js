import React, { Component } from 'react';
import Input from './form/Input';
import config from '../config';
import { withRouter } from 'react-router-dom';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.sendData = this.sendData.bind(this);
  };

  sendData(e) {
  	e.preventDefault();

  	const data = {
  		username: e.target[0].value,
  		password: e.target[1].value,
  		email: e.target[2].value
  	};

  	let that = this;

  	fetch(`${config.backendUrl}/register`, {
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST'
  	})
  	.then((res) => {
  		return res.json();
  	})
    .then((data) => {
  		that.setState({message: data.message});
  	});
  };

  goToLogin() {
    this.props.history.push('/login');
  };

  render() {
  	const message = this.state.message;

    return (
       <div className='container'>
	      <form className='registration-form' onSubmit={this.sendData}>
	      	<Input text='Username' type='text' name='username' placeholder='Your Username*' required='true'/>
	      	<Input text='Password' type='password' name='password' placeholder='Your Password*' required='true'
	      	  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters"/>
	      	<Input text='Email' type='email' name='email' placeholder='Your Email*' required='true'
	      	  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title='Invalid email'/>
	      	<Input type='submit' value='Register'/>
		  </form>
		  <button onClick={this.goToLogin.bind(this)}>Already registered?</button>
		  <div className='message'>{message}</div>
	  </div>
    );
  }

}

export default withRouter(Register);
