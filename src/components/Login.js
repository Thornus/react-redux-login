import React, {Component} from 'react';
import Input from './form/Input';
import config from '../config';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import { loginAction } from '../actions/login';
import { wrongCredsAction } from '../actions/wrongCreds';

class Login extends Component {
	constructor(props) {
		super(props);

		this.login = this.login.bind(this);
	};

	login(e) {
		e.preventDefault();

		const formData = {
			username: e.target[0].value,
			password: e.target[1].value
		};

		const loginRequest = {
			body: JSON.stringify(formData),
			headers: {
				'content-type': 'application/json'
			},
			method: 'POST'
		};

		fetch(`${config.backendUrl}/login`, loginRequest)
			.then(res => res.json())
			.then(loginResponse => {
				if (loginResponse.message) {
					console.log(this.props.onWrongCreds);
					this.props.onWrongCreds(loginResponse.message);
				} else if(loginResponse.token) {
					document.cookie = `token=${loginResponse.token}`;

					const headers = new Headers({});
					headers.set('Content-type', 'application/json');
					headers.set('Authorization', loginResponse.token);

					fetch(`${config.backendUrl}/user?username=${formData.username}`, {
						headers,
						method: 'GET'
					})
						.then(res => res.json())
						.then(data => {
							if (data.username) {
								this.props.onLogin({
									loggedIn: true,
									username: formData.username,
									email: data.formData
								});
							}
						});
				}
			});
	};

	goToRegister() {
		this.props.history.push('/register');
	};

	goToUserPage() {
		this.props.history.push('/user');
	}

	render() {
		if (this.props.loggedIn) {
			this.goToUserPage.bind(this)();
		}

		const message = this.props.message ? this.props.message : null;
		return (
			<div className='container'>
				<form className='login-form' onSubmit={this.login}>
					<Input text='Username' type='text' name='username'/>
					<Input text='Password' type='password' name='password'/>
					<Input type='submit' value='Login'/>
				</form>
				<button onClick={this.goToRegister.bind(this)}>Don't have an account?</button>
				<div className='message'>{message}</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
  return {
		loggedIn: state.login.loggedIn,
		message: state.login.message
	};
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: loginData =>	dispatch(loginAction(loginData)),
	onWrongCreds: message => dispatch(wrongCredsAction(message))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
